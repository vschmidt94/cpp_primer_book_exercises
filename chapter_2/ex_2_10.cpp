#include <iostream>
std::string global_str;
int global_int;

// C++ initializes global vars, but not local
int main() {
    std::string local_str;
    int local_int;

    std::cout << "global_int: " << global_int << std::endl;
    std::cout << "local_int: " << local_int << std::endl;

    return 0;
}