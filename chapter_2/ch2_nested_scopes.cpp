#include <iostream>

// demonstrating local and global scope. Bad style to reuse
// a global variable name like this.

int reused = 42; // global

int main() {
    int unique = 0;  // local variable.

    std::cout << "reused = " << reused << ", unique = " << unique << std::endl;

    // now declare a local version of reused
    // note we get the local from here
    int reused = 123;
    std::cout << "reused = " << reused << ", unique = " << unique << std::endl;

    // but we can access global using scope operator
    std::cout << "reused = " << ::reused << ", unique = " << unique << std::endl;
}