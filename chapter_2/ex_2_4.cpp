#include <iostream>

int main() {
    // signed and unsigned interactions
    unsigned u1 = 10, u2 = 42;
    std::cout << u2 - u1 << std::endl;  // expected 32
    std::cout << u1 - u2 << std::endl;  // expect some big number

    int i1 = 10, i2 = 42;
    std::cout << i2 - i1 << std::endl;  // expected 32
    std::cout << i1 - i2 << std::endl;  // expect -32

    std::cout << i1 - u1 << std::endl;  // expect 0
    std::cout << u1 - i1 << std::endl;  // expect 0


    return 0;
}


