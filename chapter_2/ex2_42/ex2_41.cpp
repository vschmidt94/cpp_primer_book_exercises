// Exercise 2.41 from book
// Use the header file to make use of Sales_data class (struct) object

// Personal summary: Mainly learned I needed better compiler flags set for C++11,
// but also beneficial just to force myself to not skip over sections of the 
// text.  Hard to break habits of coding conventions used @ work.

// Sample data for input from book should be:
// 0-201-78345-X 3 20.00
// 0-201-78345-X 2 25.00

#include <iostream>
#include <string>

#include "Sales_data.hpp"

int main() {
    Sales_data data1, data2;   
    double price = 0.0;   

    // Read in user input to get suggessted input lines above
    std::cout << "Please enter data for book 1: " << std::endl;
    std::cin >> data1.bookNo >> data1.units_sold >> price;
    data1.revenue = data1.units_sold * price;

    std::cout << "Please enter data for book 2: " << std::endl;
    std::cin >> data2.bookNo >> data2.units_sold >> price;
    data2.revenue = data2.units_sold * price;

    // check if the transaction is for the same ISBN.
    // if so, print the sum
    if( data1.bookNo == data2.bookNo ) {
        unsigned totalCount = data1.units_sold + data2.units_sold;
        double totalRevenue = data1.revenue + data2.revenue;
        
        // print the result
        std::cout << "For ISBN " << data1.bookNo << ": Total Sold = " << totalCount << ", Total Revenue: $ " << totalRevenue << std::endl;
    } else {
        std::cout << "The ISBN numbers did not match." << std::endl;
    }
}