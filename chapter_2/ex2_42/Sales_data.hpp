// Exercises 2.41, 2.42
// Supposed to re-write the header to use header guards, 
// but looks like the trivial example in book is what is 
// needed.
// Doing it anyways just in case I'm not getting the 
// point here.

#ifndef SALES_DATA_HPP
#define SALES_DATA_HPP

#include <string>

struct Sales_data {
    std::string bookNo;
    unsigned units_sold = 0;
    double revenue = 0.0;
};

#endif 